package Example;

import java.io.File;
import java.io.IOException;

import quicktime.QTException;
import quicktime.app.view.QTFactory;
import quicktime.io.OpenMovieFile;
import quicktime.io.QTFile;
import quicktime.qd.QDException;
import quicktime.qd.QDGraphics;
import quicktime.qd.QDRect;
import quicktime.std.StdQTConstants;
import quicktime.std.StdQTException;
import quicktime.std.image.CSequence;
import quicktime.std.image.CodecComponent;
import quicktime.std.image.CompressedFrameInfo;
import quicktime.std.image.GraphicsImporter;
import quicktime.std.image.ImageDescription;
import quicktime.std.image.QTImage;
import quicktime.std.movies.Movie;
import quicktime.std.movies.Track;
import quicktime.std.movies.media.VideoMedia;
import quicktime.util.QTHandle;
import quicktime.util.RawEncodedImage;


public class Example {

	private int VIDEO_TRACK_WIDTH	= 800;
	private int VIDEO_TRACK_HEIGHT = 600;
	private int VIDEO_TRACK_VOLUME = 0;
	private int KEY_FRAME_RATE		= 24;
	private int CODEC_TYPE;


	public Example(String filename, int width, int height, int frameRate, String directory) throws QTException {
			VIDEO_TRACK_HEIGHT = height;
			VIDEO_TRACK_WIDTH = width;
			KEY_FRAME_RATE = frameRate;
			CODEC_TYPE = StdQTConstants.kAnimationCodecType;


			QTFile movFile;
			try {
				//movFile = new QTFile(QTFactory.findAbsolutePath(filename));
				movFile = new QTFile(filename);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				movFile = null;
				e.printStackTrace();
			}
			
			Movie movie = Movie.createMovieFile(movFile, StdQTConstants.kMoviePlayer, StdQTConstants.createMovieFileDeleteCurFile | StdQTConstants.createMovieFileDontCreateResFile);
			System.out.println("Created Movie");

			Track videoTrack = movie.addTrack(VIDEO_TRACK_WIDTH, VIDEO_TRACK_HEIGHT, VIDEO_TRACK_VOLUME);
			VideoMedia videoMedia = new VideoMedia(videoTrack, KEY_FRAME_RATE);

			File[] files = new File(directory).listFiles();

			addSamples(KEY_FRAME_RATE, videoTrack, videoMedia, files);
			saveClip(movFile, movie);
	}


	private void saveClip(QTFile movFile, Movie movie) throws QTException, StdQTException {
			OpenMovieFile omf = OpenMovieFile.asWrite(movFile);
			movie.addResource(omf, StdQTConstants.movieInDataForkResID, movFile.getName());
			System.out.println("Done");
	}


	private void addSamples(int timeScale, Track videoTrack, VideoMedia videoMedia, File[] files) throws QTException, StdQTException, QDException {
		// get a GraphicsImporter
		QDGraphics gw = new QDGraphics(new QDRect(0, 0, VIDEO_TRACK_WIDTH, VIDEO_TRACK_HEIGHT));
		// figure out per-frame offsets
		QDRect gRect = new QDRect(0, 0, VIDEO_TRACK_WIDTH, VIDEO_TRACK_HEIGHT);

		videoMedia.beginEdits();
		// reserve an image with enough space to hold compressed image
		// this is needed by the last arg of CSequence.compressFrame
		int rawImageSize = QTImage.getMaxCompressionSize(gw, gRect, gw.getPixMap().getPixelSize(), StdQTConstants.codecNormalQuality, CODEC_TYPE, CodecComponent.anyCodec);
		QTHandle imageHandle = new QTHandle(rawImageSize, true);
		imageHandle.lock();
		RawEncodedImage compressedImage = RawEncodedImage.fromQTHandle(imageHandle);
		
		CSequence seq = new CSequence(gw, gRect, gw.getPixMap().getPixelSize(), CODEC_TYPE, CodecComponent.bestFidelityCodec, StdQTConstants.codecNormalQuality, StdQTConstants.codecNormalQuality, KEY_FRAME_RATE, null, 0);
		ImageDescription imgDesc = seq.getDescription();

		for (File file : files) {
			System.out.println(file.getName());
			GraphicsImporter importer = new GraphicsImporter(new QTFile(file));
			importer.setGWorld(gw, null);
			importer.draw();
			CompressedFrameInfo cfInfo = seq.compressFrame(gw, gRect,
			StdQTConstants.codecFlagUpdatePrevious, compressedImage);
			boolean syncSample = (cfInfo.getSimilarity() == 0);
			videoMedia.addSample(imageHandle, 0, cfInfo.getDataSize(), timeScale / KEY_FRAME_RATE, imgDesc, 1, (syncSample ? 0 : StdQTConstants.mediaSampleNotSync));

		}

		videoMedia.endEdits();
		videoTrack.insertMedia(0, 0, videoMedia.getDuration(), 1);
		System.out.println("inserted media into video track");
	}
}
